import { register } from 'register-service-worker';
import { Notify } from 'quasar';

// The ready(), registered(), cached(), updatefound() and updated()
// events passes a ServiceWorkerRegistration instance in their arguments.
// ServiceWorkerRegistration: https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration

register(process.env.SERVICE_WORKER_FILE, {
  // The registrationOptions object will be passed as the second argument
  // to ServiceWorkerContainer.register()
  // https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerContainer/register#Parameter

  // registrationOptions: { scope: './' },
  ready(/* registration */) {
    console.log(
      'La aplicación está siendo servida desde el caché por el service worker.'
    );
  },

  registered(/* registration */) {
    console.log('Service worker registrado correctamente.');
  },

  cached(/* registration */) {
    console.log(
      'El contenido ha sido almacenado en caché para su uso sin conexión.'
    );

    Notify.create({
      type: 'positive',
      timeout: 1000,
      message: 'Descarga del sitio completada',
    });
  },

  updatefound(/* registration */) {
    console.log('Nueva actualización encontrada. Descargando...');
    Notify.create({
      progress: true,
      spinner: true,
      type: 'positive',
      multiLine: true,
      timeout: 10000,
      message:
        'Nueva actualización encontrada, la descarga tardara unos segundos, por favor aguarde',
    });
  },

  updated(/* registration */) {
    console.log('Nueva versión disponible. Por favor, actualice la página.');
    // Emitir un evento personalizado para notificar a la aplicación que hay una actualización disponible
    Notify.create({
      progress: true,
      spinner: true,
      type: 'info',
      timeout: 5000,
      message: 'Reiniciando Aplicacion ...',
    });

    setTimeout(() => {
      window.location.reload();
    }, 2000);
  },

  offline() {
    console.log(
      'No hay conexión a internet. La aplicación está en modo sin conexión.'
    );

    Notify.create({
      progress: true,
      spinner: true,
      type: 'negative',
      timeout: 10000,
      message: 'No hay conexión a internet',
    });
  },

  error(err) {
    console.error('Error durante el registro del service worker:', err);
  },
});
