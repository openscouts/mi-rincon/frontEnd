FROM node:16-alpine
RUN apk add git
RUN echo 'Definiendo directorio de trabajo'
WORKDIR /usr/src/app

RUN echo 'Instalando Dependencias'
COPY package.json .
RUN npm install

RUN echo 'Copiando el codigo de la aplicacion'
COPY . .
RUN echo 'Compilando aplicacion'

RUN npm install -g @quasar/cli
### RUN quasar build --mode pwa
RUN quasar build -m pwa

RUN echo 'Creando la imagen final, con el codigo compilado'
FROM nginx:alpine
WORKDIR /usr/share/nginx/html
COPY --from=0 /usr/src/app/dist/pwa .
