import { boot } from 'quasar/wrappers';

export default boot(({ app }) => {
  app.config.globalProperties.$getRama = (value: string | null) => {
    switch (value) {
      case 'T':
      case 'Castores':
        return {
          texto: 'Castores',
          color: 'deep-orange',
          icon: 'fas os-saac-oms',
        };
      case 'L':
      case 'Lobatos y Lobeznas':
        return {
          texto: 'Lobatos y Lobeznas',
          color: 'amber',
          icon: 'fas os-saac-insignia-lyl',
        };
      case 'S':
      case 'Scouts':
        return {
          texto: 'Scouts',
          color: 'green',
          icon: 'fas os-saac-insignia-scout',
        };
      case 'C':
      case 'Caminantes':
        return {
          texto: 'Caminantes',
          color: 'blue',
          icon: 'fas os-saac-insignia-caminantes',
        };
      case 'R':
      case 'Rovers':
        return {
          texto: 'Rovers',
          color: 'red',
          icon: 'fas os-saac-insignia-rover',
        };
      case 'G':
      case 'Gestión institucional':
        return {
          texto: 'Gestión institucional',
          color: 'purple',
          icon: 'fas os-funcion-dirigente',
        };
    }
    return {
      texto: 'Sin Rama',
      color: 'blue-grey      ',
      icon: 'fas os-funcion-saac',
    };
  };
});
