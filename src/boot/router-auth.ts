import { useUserStore } from 'src/stores/user';
import { boot } from 'quasar/wrappers';
import { showErrorNotification } from 'src/functions/function-show-notifications';

export default boot(({ store, router }) => {
  const user = useUserStore(store);
  const rutas = ['/login', '/registro'];
  router.beforeEach((to) => {
    if (!user.loggedIn) {
      if (!rutas.includes(to.path)) {
        return router.push({ path: '/login' });
      }
    }

    if (user.loggedIn && ['/login'].includes(to.path)) {
      if (
        !user.details.login.datos_completos &&
        !['/perfil'].includes(to.path) &&
        !['/logout'].includes(to.path) &&
        !['/'].includes(to.path)
      ) {
        showErrorNotification(
          'Usted debe terminar el completado de datos personales'
        );
        return router.push({ path: '/perfil' });
      }

      // Si los datos estan todos OK .. lo envio al home !
      return router.push({ path: '/' });
    }

    if (to.meta.acl === undefined || to.meta.acl === null) {
      console.debug('No se validan Permisos');
      return;
    }

    if (!user.validarAcl(to.meta.acl, true)) {
      console.debug('No tiene permisos');
      showErrorNotification(
        'Esta ingresando a un modulo que no tiene permisos'
      );
      return router.push({ path: '/' });
    }
  });
});
