'use strict';

import VueApexCharts from 'vue3-apexcharts';
import * as lodash from 'lodash';
import { boot } from 'quasar/wrappers';

// https://nuxtjs.org/docs/concepts/context-helpers/
export default boot(({ app }) => {
  app.use(VueApexCharts);
  app.config.globalProperties.$_ = lodash;
  app.config.globalProperties.$lodash = lodash;
});
