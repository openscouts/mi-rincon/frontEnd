import { boot } from 'quasar/wrappers';
import axios, {
  InternalAxiosRequestConfig,
  AxiosInstance,
  AxiosError,
  AxiosResponse,
  AxiosHeaders,
} from 'axios';
import { useUserStore } from 'src/stores/user';
import {
  showSuccessNotification,
  showErrorNotification,
} from 'src/functions/function-show-notifications';
declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $axios: AxiosInstance;
  }
}
import validacion from './Validacion';

/// =>>>> https://github.com/training-yoyosan/example-frontend/blob/master/src/boot/axios.js
// Tenga cuidado al usar SSR para la contaminación del estado de solicitudes cruzadas
// debido a la creación de una instancia de Singleton aquí;
// Si algún cliente cambia esta instancia (global), podría ser un
// buena idea mover la creación de esta instancia dentro del
// función "export default () => {}" a continuación (que se ejecuta individualmente
// para cada cliente)

const api = axios.create({
  baseURL: process.env.ENV_API_HOST,
  timeout: 20000,
  headers: {
    Accept: 'application/json',
  },
});

// https://dev.to/charlintosh/setting-up-axios-interceptors-react-js-typescript-12k5

export default boot(async ({ app, store, router }) => {
  api.defaults.withCredentials = true;
  const user = useUserStore(store);

  const onRequest = (
    config: InternalAxiosRequestConfig
  ): InternalAxiosRequestConfig => {
    if (!config.headers) {
      config.headers = <AxiosHeaders>{};
    }
    if (user.token != undefined && config != undefined) {
      config.headers.authorization = 'Bearer ' + user.token;
    }
    validacion.flush(); // Gestion de errores !! antes que nada .. BORRO TODO !!
    return config;
  };

  const onRequestError = (error: AxiosError): Promise<AxiosError> => {
    // console.error(`[request error] [${JSON.stringify(error)}]`);
    return Promise.reject(error);
  };

  const onResponse = (response: AxiosResponse): AxiosResponse => {
    // Cualquier código de estado que se encuentre dentro del rango de 2xx hace que esta función se active
    // Hacer algo con los datos de respuesta
    if (response.data.success) {
      if (response.data.success != 'OK') {
        showSuccessNotification(response.data.success);
      }
    }

    return response;
  };

  const onResponseError = (error: AxiosError): Promise<AxiosError> => {
    if (error.response == undefined) {
      return Promise.reject(error);
    }

    switch (error.response.status) {
      case 400:
        // PENDIENTE, tengo que definir una interface de repuesta de erro ... que devuelva un item error y un irem detalle .. yo que se ..algo asi
        showErrorNotification(error.response.data.error);
        break;
      case 401:
        if (window.location.pathname !== '/login') {
          //  app.$noti('Debe hacer login', 'danger', 'Session Finalizado');
          //redirect('/login/logout');
          showErrorNotification('Usted require hacer nuevamente login!');
          return router.push({ path: '/logout' });
        }
        break;
      case 422:
        console.info('Se genero un error de validacion de formulario');
        validacion.flush(); // Gestion de errores !! antes que nada .. BORRO TODO !!
        if (error.response.data.errors) {
          validacion.fill(error.response.data.errors);
        }
        break;

      default:
        if (error.response.data.message) {
          showErrorNotification(error.response.data.message);
        }
        if (error.response.data.error) {
          showErrorNotification(error.response.data.error);
        }
        break;
    }
    // Cualquier código de estado que se encuentre fuera del rango de 2xx hace que esta función se active
    // antes estaba en reject, pero no se ejeuctaban los finally de axios, con resolve si se ejecuta
    return Promise.reject(error);
  };

  api.interceptors.request.use(onRequest, onRequestError);
  api.interceptors.response.use(onResponse, onResponseError);

  // for use inside Vue files (Options API) through this.$axios and this.$api
  app.config.globalProperties.$axios = axios;

  // ^ ^ ^ esto le permitirá usar this. $ axios (para el formulario de API de opciones de Vue)
  // por lo que no necesariamente tendrá que importar axios en cada archivo vue

  app.config.globalProperties.$api = api;
  // ^ ^ ^ esto le permitirá usar this. $ api (para el formulario de API de opciones de Vue)
  // para que pueda realizar solicitudes fácilmente contra la API de su aplicación
});

export { axios, api, validacion };
