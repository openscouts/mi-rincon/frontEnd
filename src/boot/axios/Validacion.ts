import { reactive } from 'vue';

interface Errors {
  [key: string]: string[];
}

class Validacion {
  // private errors: Errors;
  // si no lo hago reactivo, no actualiza correctamente ... por fallar la reactividad !! jaja
  data = reactive({
    errors: {} as Errors,
  });

  constructor() {
    this.data.errors = {};
  }

  public has(key: string): boolean {
    return this.data.errors.hasOwnProperty(key);
  }

  public first(key: string): string | undefined {
    if (this.has(key)) {
      return this.data.errors[key][0];
    }
  }

  public get(key: string): string[] | undefined {
    if (this.has(key)) {
      return this.data.errors[key];
    }
  }

  public all(): Errors {
    return this.data.errors;
  }

  public fill(values: Errors): void {
    this.data.errors = values;
  }

  public flush(): void {
    this.data.errors = {};
  }
}

export default new Validacion();
