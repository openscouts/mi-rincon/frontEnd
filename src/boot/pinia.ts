//https://github.com/vuejs/pinia/discussions/553

import { boot } from 'quasar/wrappers';
//import { createPinia } from 'pinia';
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate';

export default boot(({ app, store }) => {
  store.use(piniaPluginPersistedstate);
  app.use(store);
});
