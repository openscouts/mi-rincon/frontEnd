import { boot } from 'quasar/wrappers';
import { useUserStore } from 'src/stores/user';

export default boot(({ store, app }) => {
  const user = useUserStore(store);

  app.config.globalProperties.$isCan = (value: string) =>
    user.validarAcl(value, true);

  app.config.globalProperties.$isFormador = () => user.details.is.formador;
  app.config.globalProperties.$isSoloFormador = () =>
    user.details.is.soloformador;
});
