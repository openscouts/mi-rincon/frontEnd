import { boot } from 'quasar/wrappers';
import moment from 'moment';

export default boot(({ app }) => {
  app.config.globalProperties.$filters = {
    DateTimeFull(value: Date | string) {
      const fecha = moment(value);
      if (!fecha.isValid() || value === '' || value === undefined) {
        return '';
      }
      return fecha.format('DD/MM/YYYY  H:mm:ss.SSS');
    },

    DateTime(value: Date | string) {
      const fecha = moment(value);
      if (!fecha.isValid() || value === '' || value === undefined) {
        return '';
      }
      return fecha.format('DD/MM/YYYY  H:mm');
    },
    Date(value: Date | string) {
      const fecha = moment(value);
      if (!fecha.isValid() || value === '' || value === undefined) {
        return '';
      }
      return fecha.format('DD/MM/YYYY');
    },
  };
});
