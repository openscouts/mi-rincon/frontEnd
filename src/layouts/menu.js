export default [
  {
    name: 'Educadores',
    icon: 'fas fa-house-user',
    url: '/main',
    children: [
      {
        name: 'Inscripciones',
        url: '/main/inscripciones',
        icon: 'fas os-accion-evento',
      },
      {
        name: 'Mi Formacion',
        url: '/main/miformacion',
        icon: 'fas os-area-ams',
      },
    ],
  },
  {
    name: 'Eventos',
    icon: 'fas os-accion-evento',
    acl: 'eventos',
    children: [
      {
        name: 'Administracion',
        url: '/eventos/administracion',
        icon: 'fas os-accion-evento-list',
        acl: 'eventos.listados',
      },
      {
        name: 'Creacion',
        url: '/eventos/creacion',
        icon: 'fas os-accion-evento-add',
        acl: 'eventos.alta',
      },
      {
        name: 'Lugares',
        url: '/eventos/lugares',
        icon: 'fas os-accion-lugar',
        acl: 'eventos.lugares',
      },
    ],
  },
  {
    name: 'Formación',
    icon: 'fas os-area-ams',
    acl: 'formacion',
    children: [
      {
        name: 'Experiencias',
        url: '/formacion/experiencias',
        icon: 'fas os-scout-saludo',
        acl: 'formacion.experiencias',
      },
      {
        name: 'Insignia de Madera',
        url: '/formacion/insignia_de_madera',
        icon: 'fas os-accion-im',
        acl: 'formacion.insignia_de_madera',
      },
      {
        // experiencia participaron los formadores .. es menu personaliado para los formadores.
        name: 'Formadores',
        url: '/formacion/formadores',
        icon: 'groups_3',
        acl: 'formacion.formadores',
      },
      {
        name: 'Cursantes',
        url: '/formacion/cursantes',
        icon: 'groups_2',
        acl: 'formacion.cursantes',
      },

      {
        name: 'Certificados',
        url: '/formacion/certificados',
        icon: 'history_edu',
        acl: 'formacion.certificados',
      },

      /*
      {
        name: 'Carga Manual',
        url: '/formacion/carga_manual',
        icon: 'fas os-camping-llano',
        acl: 'formacion.carga_manual',
      },*/
    ],
  },
  {
    name: 'Finanzas',
    icon: 'fas fa-hand-holding-usd',
    acl: 'finanzas',
    children: [
      {
        name: 'Recibos',
        url: '/finanzas/recibos',
        icon: 'fas fa-hand-holding-usd',
        acl: 'finanzas.recibos',
      },
    ],
  },
  {
    name: 'Membresia',
    icon: 'fas fa-user-friends',
    acl: 'membresia',
    children: [
      {
        name: 'Personas',
        url: '/membresia/personas',
        icon: 'fas os-area-programa',
        acl: 'membresia.personas',
      },
      {
        name: 'Afiliaciones',
        url: '/membresia/afiliacion',
        icon: 'fas os-saac-isologo',
        acl: 'membresia.afiliacion',
      },
      {
        name: 'Funciones',
        url: '/membresia/funciones',
        icon: 'fab fa-angellist',
        acl: 'membresia.funciones',
      },
    ],
  },

  {
    name: 'ADMINISTRADOR',
    icon: 'fas fa-users-cog',
    acl: 'administrador',
    children: [
      {
        name: 'Novedades',
        url: '/administrador/novedades',
        icon: 'fas fa-bell',
        acl: 'administrador.novedades',
      },
      {
        name: 'Usuarios',
        url: '/administrador/usuarios',
        icon: 'fas fa-users',
        acl: 'administrador.usuarios',
      },
      {
        name: 'Registro Pendiente',
        url: '/administrador/registros',
        icon: 'fas fa-pencil-ruler',
        acl: 'administrador.permisos',
      },
      {
        name: 'Permisos',
        url: '/administrador/permisos',
        icon: 'fas fa-toolbox',
        acl: 'administrador.permisos',
      },
      {
        name: 'Jobs',
        url: '/administrador/jobs',
        icon: 'fas fa-hammer',
        acl: 'administrador.permisos',
      },
    ],
  },
];
