import { PersonaInterface } from './PersonasInterface';

export interface EventoInterface {
  id?: number;
  uuid: string;
  organismos_id: number;
  nombre: string;
  eventos_tipo_id: number | undefined;
  eventos_area_id: number | undefined;
  ams_experiencias_id: number | undefined;
  f_inicio_evento: string;
  f_fin_evento: string;
  f_cierre_inscripcion: string;
  descripcion: string;
  costo: number;
  inscripcion?: string;
  participa: string;
  remera?: string;
  beneficiarios: string;
  rama: string[];
  modalidad: string;
  responsable: string;
  responsable_id: number;
  responsable_uuid: string;
  carga_horaria: number;
  eventos_lugar_id: number;
  eventos_estado_id: number;
  cupo: number;
  cupo_externo?: number;
  lista_espera?: string;
  crea_ams?: string;
  genera_certificado: string;
  autorizado?: string;
  autorizado_by?: number;
  autorizado_at?: string;
  archivado?: string;
  created_at?: string;
  created_by?: number;
  updated_at?: string;
  updated_by?: number;
  deleted_at?: string;
  deleted_by?: number;
}

// Se usa en /eventos/administracion/detalle/
export interface EventoDetalleInterface {
  uuid: string;
  evento: string;
  costo: number;
  cupo: number;
  pauta: string;
  evento_inicio: string;
  evento_fin: string;
  cierre_inscripcion: string;
  lugar: string;
  tipo: string;
  area: string;
  area_codigo: string;
  organismo: string;
  eventos_estado_id: number;
  beneficiarios: boolean;
  ramas: string;
  remera: boolean;
  crea_ams: boolean;
  curso_uuid: string;
  responsable: string;
  autorizado: boolean;
  soy_propietario: boolean;
  rama_obligatorio: boolean;
  borrado: boolean;
}

export interface EventoInscripcionesInterface {
  uuid: string;
  evento: string;
  costo: number;
  pauta: string;
  fecha_evento: string;
  cierre_inscripcion: string;
  area: string;
  area_codigo: string;
  organismo: string;
  inscripto_uuid: boolean;
  rama: boolean;
  modalidad: string;
  estado: string;
  revalida_ams: boolean;
  beneficiarios: boolean;
  cupo: boolean;
  cupo_cantidad: number;
}

export interface EventosInscriptosInterface {
  uuid: string;
  rama: string;
  f_inscripcion: string;
  persona: PersonaInterface;
}

export interface ComboAreaInterface {
  id: number;
  nombre: string;
  codigo: string;
  icono: string;
  color: string;
  beneficiarios: string;
  remera: string;
  rama: string;
  cupo: string;
  lugar: string;
  estado: string;
  eventos_tipo: {
    id: number;
    nombre: string;
    AMS: string;
    deleted_at: string;
  }[];
}

export interface ComboLugarInterface {
  id: number;
  uuid: string;
  organismos_id: number;
  nombre: string;
  direccion: string;
  localidad: string;
  telefono?: string;
  comentario: string;
  maps: string;
  created_at: string;
  created_by: number;
  updated_at: string;
  updated_by?: number;
  deleted_at?: string;
  deleted_by?: number;
}

export interface ComboExperienciasInterface {
  id: number;
  codigo: string;
  nombre: string;
  rama?: string;
  orden?: number;
  instancia?: string;
  esquema?: number;
  ams_instancias_id?: number;
  tipo?: string;
  visible: string;
  estado: string;
}

export interface LugaresInterface {
  id?: number;
  uuid?: string;
  organismos_id?: number;
  nombre: string;
  direccion: string;
  localidad: string;
  telefono: string;
  comentario: string;
  maps: string;
}
