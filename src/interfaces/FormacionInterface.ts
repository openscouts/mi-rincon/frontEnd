import { OrganismosInterface } from './VariosInterface';
import { PersonaBasicaInterface, PersonaInterface } from './PersonasInterface';

export interface FormacionParticipantesInterface {
  uuid: string;
  rama: string;
  rama_obligatoria: boolean;
  persona: PersonaInterface;
  estado: string;
  estado_id: number;
  libro_folio: string;
  libro: number;
  folio: number;
  trabajo_uuid: string[];
}

// se usa en => formacion/cursantes/detalle/
export interface FormacionFullParticipantesInterface {
  uuid: string;
  organismo: string;
  organismo_codigo: string;
  experiencia: string;
  fecha: string;
  rama: string;
  solicita_rama: boolean;
  origen: string;
  observaciones: string;
  estado: string;
  libro_folio: string;
  fehaciente: string;
  certificado: boolean;
}

export interface ExperienciaInterface {
  archivado: string;
  nombre: string;
  fecha: string;
  uuid: string;
  responsable: string;
  organismo: OrganismosInterface;
  solicita_rama: boolean;
}

export interface InsigniasMaderaInterface {
  uuid: string;
  otorgada: string;
  persona: PersonaInterface;
  nro_registro: number;
  linea: string;
  rama: string;
  anio: string;
  observacion: string;
  vencimiento: string;
  f_solicitud: string;
}

export interface ExperienciasPendientesInterface {
  uuid: string;
  organismo: string;
  organismo_codigo: string;
  experiencia: string;
  fecha: string;
  responsable: string;
  pendientes: number;
}

export interface ExperienciasListsadoInterface {
  id: number;
  uuid: string;
  eventos_id: number;
  ams_experiencias_id: number;
  organismos_id: number;
  nombre: string;
  fecha: string;
  responsable_id: number;
  responsable: string;
  origen: string;
  ams_cursos_origen_id: number;
  archivado: string;
  organismos: OrganismosInterface;
}

export interface CargaManualFormacionInterface {
  uuid: string;
  organismo: string;
  organismo_codigo: string;
  experiencia: string;
  fecha: string;
  rama: string;
  persona: PersonaBasicaInterface;
  estado: string;
  libro_folio: string;
  observaciones: CargaManualFormacionObservacioneInterface[];
}

export interface CargaManualFormacionObservacioneInterface {
  fecha: string;
  creador: string;
  observacion: string;
}
