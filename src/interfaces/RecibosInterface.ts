export interface ReciboAltaInterface {
  numero: string;
  tipo_destinatario: string;
  organismo_by: number | null;
  persona_by: string | null;
  concepto_id: number;
  observaciones: string;
  evento_id: number | null;
  evento_costo: number;
  fecha: string | null;
  calculo: string;
  total: number;
  detalles: ReciboAltaDetalleInterface[] | [];
}

export interface ReciboAltaDetalleInterface {
  persona_uuid: string | null;
  // organismo_id: number;
  // detalle: string;
  calculo: string;
  monto: number;
}

export interface ConceptosInterface {
  id: string;
  nombre: string;
}

export interface EventosInterface {
  id: number;
  costo: number;
  nombre: string;
}

//   persona: PersonaInterface;
