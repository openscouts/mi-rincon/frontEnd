import { RamaInterface } from './VariosInterface';

export interface PersonaBasicaInterface {
  uuid: string;
  documento: number;
  apellidoynombre: string;
  tipo: string;
  categoria: string;
  afiliacion: boolean;
  funciones: FuncionesInterface[];
  rama: string;
}

export interface PersonaInterface {
  uuid: string;
  documento: number;
  apellidoynombre: string;
  nombre: string;
  apellido: string;
  adulto: boolean;
  categoria: string;
  nacimiento: string;
  afiliacion: boolean;
  funciones?: FuncionesInterface[];
  datos?: PersonaDatosInterface;
  rama: string;
}

// Sin uso !!
export interface PersonEdicionInterface {
  id: number;
  uuid: string;
  documento: number;
  nacionalidad: string;
  apellido: string;
  nombre: string;
  apellidoynombre: string;
  domicilio: string;
  codigo_postal: number;
  localidad: string;
  provincias_id: number;
  f_nacimiento: string;
  edad: number;
  sexo: string;
  estado_civil_id: number;
  telefono: string;
  celular: string;
  email: string;
  religion_id: number;
  religion_det: string;
  estudios: string;
  titulo: string;
  empresa_donde_trabaja: string;
  idioma: string;
  promesa: string;
  categoria_id: number;
  tipo: string;
  rama: string;
  necesidad_especial: string;
  tipo_de_necesidad_especial: string;
  totem: string;
  zona: number;
  fecha_1er_afiliacion: string;
  afiliacion_id: number;
  created_at: string;
  created_by: number;
  updated_at: string;
  updated_by: number;
  deleted_at: string;
  deleted_by: string;
  certificado: string;
  certificado_json: string;
}

export interface PersonaFullInterface {
  uuid: string;
  documento: number;
  nacionalidad: string;
  apellido: string;
  nombre: string;
  apellidoynombre: string;
  domicilio: string;
  codigo_postal: number;
  localidad: string;
  provincias: string;
  nacimiento: string;
  edad: number;
  sexo: string;
  estado_civil: string;
  telefono: string;
  celular: string;
  email: string;
  religion: string;
  religion_detalle: string;
  estudios: string;
  titulo: string;
  trabajo: string;
  idioma: string;
  promesa: string;
  categoria: string;
  rama: string;
  rama_codigo: string;
  necesidad_especial: boolean;
  tipo_de_necesidad_especial: string;
  totem: string;
  fecha_1er_afiliacion: string;
  ultima_actualizacion: string;
  adulto: boolean;
  afiliacion: boolean;
  certificado: string;
}

export interface PersonaCumpleAniosInterface {
  uuid: string;
  documento: number;
  apellidoynombre: string;
  tipo: string;
  afiliacion: string;
  funciones: FuncionesInterface[];
  cumpleanios: string;
  cumple: number;
}

export interface PersonaDatosInterface {
  alimentacion_especial: string;
  alimentacion_especial_detalle: string;
  celular: string;
  telefono: string;
  email: string;
  profesion: string;
  actualizacion: string;
}

export interface FuncionesInterface {
  uuid: string;
  zona: number;
  distrito: number;
  organismo: string;
  organismo_codigo: string;
  organismo_full: string;
  funcion: string;
  origen: string;
  rama: RamaInterface;
  beneficiario: boolean;
}
