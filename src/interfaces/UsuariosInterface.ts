import { FuncionesInterface } from './PersonasInterface';

export interface UsuariosInterface {
  uuid: string;
  documento: number;
  apellidoynombre: string;
  nombre: string;
  apellido: string;
  adulto: boolean;
  categoria: string;
  nacimiento: string;
  afiliacion: boolean;
  string: FuncionesInterface[];
  rama: string;
}
