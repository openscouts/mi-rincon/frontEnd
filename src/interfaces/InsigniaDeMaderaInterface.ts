import { PersonaBasicaInterface } from './PersonasInterface';

export interface ImListadoInterface {
  persona: PersonaBasicaInterface;
  ims: Im[];
}

export interface Im {
  uuid: string;
  otorgada: string;
  nro_registro: number;
  linea: string;
  rama: string;
  anio: string;
  observacion: string;
  vencimiento: string;
  f_solicitud: string;
}

export interface ImDetalleInterface {
  uuid: string;
  otorgada: string;
  persona: PersonaBasicaInterface;
  nro_registro: number;
  linea: string;
  rama: string;
  anio: string;
  observacion: string;
  vencimiento: string;
  f_solicitud: string;
}
