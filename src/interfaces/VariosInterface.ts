export interface RamaInterface {
  rama: string;
  color: string;
}

export interface OrganismosInterface {
  id: number;
  zona: number;
  distrito: number;
  organismo: string;
  codigo: string;
  full: string;
}

export interface GraficoTortaInterface {
  nombre: string;
  color: string;
  total: number;
}

export interface NovedadesInterface {
  id: number;
  uuid: string;
  titulo: string;
  detalle: string;
  link: string;
  tipo: string;
  zona: number;
  distrito: number;
  f_inicio: string;
  f_fin: string;
  created_at: string;
  created_by: number;
  updated_at: string;
  updated_by: number;
}
