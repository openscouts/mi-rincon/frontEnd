import { Loading, Notify } from 'quasar';

function showErrorNotification(errorMessage: string) {
  Loading.hide(); // ante la duda siempre oculto el Loading
  Notify.create({
    color: 'negative',
    position: 'top',
    message: errorMessage,
    icon: 'report_problem',
  });
}

function showSuccessNotification(message: string) {
  Loading.hide(); // ante la duda siempre oculto el Loading
  Notify.create({
    color: 'green-5',
    textColor: 'white',
    icon: 'done',
    position: 'top',
    message: message,
  });
}

export { showErrorNotification, showSuccessNotification };
