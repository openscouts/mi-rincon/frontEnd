/* eslint-disable */

import { defineStore } from 'pinia';
import { Loading, QSpinnerBall } from 'quasar';
import axios, { AxiosResponse } from 'axios';
import { Usuario } from './indexInterface';
import { useRouter } from 'vue-router';

import {
  showErrorNotification,
  showSuccessNotification,
} from 'src/functions/function-show-notifications';

export interface UsuarioInterface {
  documento?: number | null;
  password?: string | null;
  organismo?: string | null;
}

const api = axios.create({
  baseURL: process.env.ENV_API_HOST,
  timeout: 20000,
  headers: {
    Accept: 'application/json',
  },
});

export const useUserStore = defineStore('usuario', {
  persist: true, // defino que los datos son persistentes en el navergador
  state: () => ({
    loggedIn: false,
    details: <Usuario>{},
    token: null,
  }),
  getters: {},
  actions: {
    login(payload: UsuarioInterface) {
      Loading.show({
        spinner: QSpinnerBall,
        message: 'Ingresando a Mi Rincon',
      });
      api.defaults.withCredentials = true;
      api
        .get('/sanctum/csrf-cookie')
        .then(() => {
          api
            .post('/api/auth/login', payload)
            .then((response: AxiosResponse) => {
              this.loggedIn = true;
              this.token = response.data.access_token;
              api.defaults.headers.common[
                'Authorization'
              ] = `Bearer ${response.data.access_token}`;

              api
                .get('/api/user')
                .then((response: AxiosResponse) => {
                  this.details = response.data;
                  showSuccessNotification('Bienvenido a Mi Rincon!');
                  window.location.href = '/';
                })
                .catch(() => {
                  showErrorNotification('¡No estás autenticado!');
                });
            })
            .catch(() => {
              showErrorNotification('¡No se pudo realizar la autenticación!');
            });
        })
        .catch(() => {
          showErrorNotification('¡No se pudo realizar la autenticación!');
        });
    },

    updateUser() {
      api.defaults.headers.common['Authorization'] = `Bearer ${this.token}`;
      api.get('/api/user').then((response: AxiosResponse) => {
        this.details = response.data;
      });
    },

    validarAcl(acl: string, estricto: boolean) {
      if (acl === undefined) {
        return true;
      }
      if (estricto === undefined) {
        estricto = true; // Siempre es estricto !!!!!
      }

      var Permisos = [''];

      Object.entries(this.details.permisos).forEach((item) => {
        Permisos.push(item[1]);
      });

      // Si no es extricto, solo valido que exista una parte del ACL , solo lo uso para validar los permisos al armar el menu.
      if (!estricto) {
        let resultado = false;

        if (this.details.permisos === undefined) {
          return false;
        }

        Permisos.forEach((item) => {
          if (item.includes(acl)) {
            resultado = true;
          }
        });
        return resultado;
      }

      return Permisos.includes(acl);
    },

    async logout() {
      const reset = () => {
        const router = useRouter();
        this.loggedIn = false;
        this.token = null;
        window.location.href = '/';
        return router.push({ path: '/login' });
      };
      Loading.show({
        spinner: QSpinnerBall,
        message: 'Saliendo del sistema',
      });
      api
        .post('/api/logout')
        .then(() => {
          showSuccessNotification('¡Se ha desconectado!');
          reset();
        })
        .catch(() => {
          showErrorNotification('¡Sesión expirada!');
          reset();
        });
    },
  },
});
