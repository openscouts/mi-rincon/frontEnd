export interface Usuario {
  organismos: Organismos;
  persona: Persona;
  login: Login;
  permisos: string[];
  is: Is;
}

export interface Organismos {
  zona_id: number;
  zona: number;
  distrito_id: number;
  distrito: number;
  grupo_id: number;
}

export interface Persona {
  uuid: string;
  documento: number;
  apellidoynombre: string;
  nombre: string;
  apellido: string;
  adulto: boolean;
  categoria: string;
  nacimiento: string;
  afiliacion: boolean;
  datos: Datos;
  rama: string;
}

export interface Datos {
  alimentacion_especial: string;
  alimentacion_especial_detalle: string;
  celular: string;
  telefono: string;
  email: string;
  profesion: string;
  actualizacion: string;
}

export interface Login {
  organismo_id: number;
  funciones: string[];
  datos_completos: boolean;
  rama: string;
}

export interface Is {
  grupo: boolean;
  distrito: boolean;
  zona: boolean;
  region: boolean;
  nacion: boolean;
  formador: boolean;
  soloformador: boolean;
  admin: boolean;
  afiliado: boolean;
}
