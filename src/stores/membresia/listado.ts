import { defineStore } from 'pinia';
import { QTableProps } from 'quasar';
import { showSuccessNotification } from 'src/functions/function-show-notifications';

interface FiltroInterface {
  // Paginado
  sortBy: string;
  descending: boolean;
  page: number;
  rowsPerPage: number;
  rowsNumber: number;
  // Busqueda
  query: string;
  afiliado: string[]; // Afiliado / No afiliado
  tipo: string[]; // Adulto // Beneficiario
  categoria: string[]; // activo / colaborador / etc
  rama: string[];
  im: string | null;
  organismos: number[];
}

export const membresiaListadoSelect = {
  afiliado: [
    { value: 'A', label: 'Afiliados' },
    { value: 'I', label: 'Ex Miembros' },
  ],
  tipo: [
    { value: 'A', label: 'Adultos' },
    { value: 'B', label: 'Beneficiario' },
  ],
  categoria: [
    { value: 1, label: 'Activo' },
    { value: 2, label: 'Colaborador' },
    { value: 3, label: 'Cooperador' },
    { value: 4, label: 'Asesor' },
    { value: 5, label: 'Honorario' },
    { value: 8, label: 'Acompañante' },
  ],
  im: [
    { value: 'S', label: 'Es IM' },
    { value: 'N', label: 'No es IM' },
  ],
};

export const useMembresiaListadoStore = defineStore('membresia.listado', {
  persist: true, // defino que los datos son persistentes en el navergador
  state: () => ({
    filtroActivado: false,
    filtros: <FiltroInterface>{
      // Paginado
      sortBy: 'desc',
      descending: false,
      page: 1,
      rowsPerPage: 10,
      rowsNumber: 0,
      // Busqueda
      query: '',
      afiliado: ['A'], // Afiliado Activo / No afiliado Inactivo
      tipo: ['A'], // Adulto // Beneficiario
      categoria: [], // activo / colaborador / etc
      rama: [],
      im: null,
      organismos: [],
    },
  }),
  getters: {
    parametros(state) {
      // rescribo varias varibles, porque al momento de enviarlas por get, necesito que los que son array se separen x coma (,)
      return {
        ...state.filtros,
        tipo: state.filtros.tipo.join(','),
        afiliado: state.filtros.afiliado.join(','),
        rama: state.filtros.rama.join(','),
        categoria: state.filtros.categoria.join(','),
        organismos: state.filtros.organismos.join(','),
      };
    },
  },
  actions: {
    setRowTotal(total: number) {
      this.filtros.rowsNumber = total;
    },
    setPaginar(props: QTableProps) {
      this.filtros = <FiltroInterface>{
        ...this.filtros,
        ...props.pagination,
      };
    },
    resetFiltros() {
      showSuccessNotification('¡Filtros Reinicializados!');
      this.$reset();
    },
  },
});
