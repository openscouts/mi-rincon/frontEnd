import { defineStore } from 'pinia';
import { QTableProps } from 'quasar';
import { showSuccessNotification } from 'src/functions/function-show-notifications';
import { useUserStore } from 'src/stores/user';

interface FiltroInterface {
  // Paginado
  sortBy: string;
  descending: boolean;
  page: number;
  rowsPerPage: number;
  rowsNumber: number;
  // Busqueda
  tipo: string[];
  area: string[];
  estado: string[];
  query: string;
}

export const eventosListadoSelect = {
  tipo: [
    { value: 'miseventos', label: 'Mis Eventos' },
    { value: 'publicos', label: 'Eventos Publicos' },
  ],
  area: [
    { value: 'ams', label: 'Adultos en el Movimiento' },
    { value: 'otros', label: 'Otras Areas' },
  ],
  estado: [
    { value: 'activos', label: 'Activos' },
    { value: 'archivados', label: 'Archivados' },
    { value: 'borrados', label: 'Borrados' },
  ],
};

const store = useUserStore();

export const useEventosListadoStore = defineStore('eventos.listado', {
  persist: true, // defino que los datos son persistentes en el navergador
  state: () => ({
    filtroActivado: false,
    filtros: <FiltroInterface>{
      // Paginado
      sortBy: 'desc',
      descending: false,
      page: 1,
      rowsPerPage: 10, // revisar este campo porque quiero usar rowsPerPage ya que es el que usa quasar
      rowsNumber: 0,
      // Busqueda
      tipo: store.details.is.grupo ? [] : ['miseventos'],
      area: [],
      estado: ['activos'],
      query: '',
    },
  }),
  getters: {
    parametros(state) {
      // rescribo varias varibles, porque al momento de enviarlas por get, necesito que los que son array se separen x coma (,)
      return {
        ...state.filtros,
        tipo: state.filtros.tipo.join(','),
        area: state.filtros.area.join(','),
        estado: state.filtros.estado.join(','),
      };
    },
  },
  actions: {
    setRowTotal(total: number) {
      this.filtros.rowsNumber = total;
    },
    setPaginar(props: QTableProps) {
      this.filtros = <FiltroInterface>{
        ...this.filtros,
        ...props.pagination,
      };
    },
    resetFiltros() {
      showSuccessNotification('¡Filtros Reinicializados!');
      this.$reset();
    },
  },
});
