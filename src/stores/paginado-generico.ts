import { defineStore } from 'pinia';
import { QTableProps } from 'quasar';
import { showSuccessNotification } from 'src/functions/function-show-notifications';

// ESTO SOLO SE USA CUANDO SE TIENE UN query nada mas .. es bien generico

interface FiltroInterface {
  // Paginado
  sortBy: string;
  descending: boolean;
  page: number;
  rowsPerPage: number;
  rowsNumber: number;
  // Busqueda
  query: string;
}

export const usePaginadoGenericoStore = defineStore('listado.generico', {
  persist: true, // defino que los datos son persistentes en el navergador
  state: () => ({
    filtroActivado: false,
    filtros: <FiltroInterface>{
      // Paginado
      sortBy: 'desc',
      descending: false,
      page: 1,
      rowsPerPage: 10,
      rowsNumber: 0,
      // Busqueda
      query: '',
    },
  }),
  getters: {
    parametros(state) {
      // rescribo varias varibles, porque al momento de enviarlas por get, necesito que los que son array se separen x coma (,)
      return {
        ...state.filtros,
      };
    },
  },
  actions: {
    setRowTotal(total: number) {
      this.filtros.rowsNumber = total;
    },
    setPaginar(props: QTableProps) {
      this.filtros = <FiltroInterface>{
        ...this.filtros,
        ...props.pagination,
      };
    },

    resetFiltros() {
      showSuccessNotification('¡Filtros Reinicializados!');
      this.$reset();
    },
  },
});
