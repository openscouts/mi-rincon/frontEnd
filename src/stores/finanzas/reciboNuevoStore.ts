import {
  ReciboAltaDetalleInterface,
  ReciboAltaInterface,
} from 'src/interfaces/RecibosInterface';
import { defineStore } from 'pinia';
import { api } from 'src/boot/axios/index';
import { AxiosResponse } from 'axios';
import {
  ConceptosInterface,
  EventosInterface,
} from 'src/interfaces/RecibosInterface';
import { Notify } from 'quasar';
import moment from 'moment';

const nuevoDetalle: ReciboAltaDetalleInterface = {
  calculo: 'A',
  monto: 0,
  persona_uuid: null,
};

export const useReciboNuevoStore = defineStore('finanzas.reciboNuevoStore', {
  state: () => ({
    form: {
      numero: '',
      tipo_destinatario: 'O',
      organismo_by: null,
      persona_by: null,
      concepto_id: 2,
      observaciones: '',
      evento_id: null,
      evento_costo: 0,
      fecha: moment().format('YYYY/MM/DD'),
      calculo: 'A',
      total: 0,
      detalles: [{ ...nuevoDetalle }],
    } as ReciboAltaInterface,
    combos: {
      conceptos: <ConceptosInterface[]>{},
      eventos: <EventosInterface[]>{},
    },
    //   dialog: ref<boolean>(false),
    dialog: false,
    participantes: <string[]>{},
  }),

  getters: {
    doubleCount(state): number {
      return state.form.total * 2;
    },
  },
  actions: {
    getCombos() {
      return Promise.all([
        api
          .get('/api/finanzas/recibos/concepto')
          .then((response: AxiosResponse) => {
            this.combos.conceptos = <ConceptosInterface[]>response.data;
          }),

        api
          .get('/api/finanzas/recibos/listado_eventos')
          .then((response: AxiosResponse) => {
            this.combos.eventos = <EventosInterface[]>response.data;
          }),
      ]);
    },

    reiniciarFormulario() {
      this.$reset();
      this.getCombos();
    },

    getParticipantesEvento() {
      if (this.form.organismo_by === null) {
        Notify.create({
          type: 'info',
          timeout: 1000,
          message: 'Debe Definir un organismo',
        });
        return; // finalizo el proceso en este punto
      }

      if (this.form.evento_id === null) {
        Notify.create({
          type: 'info',
          timeout: 1000,
          message: 'Debe Definir un Evento',
        });
        return; // finalizo el proceso en este punto
      }

      api
        .get('/api/finanzas/recibos/listado_participantes', {
          params: {
            organismo_by: this.form.organismo_by,
            evento_id: this.form.evento_id,
          },
        })
        .then((response: AxiosResponse) => {
          if (response.data.length === 0) {
            Notify.create({
              type: 'info',
              timeout: 1000,
              message:
                'No Existen participantes para la convinacion de evento/organismo',
            });
          }

          this.participantes = <string[]>response.data;
          this.form.detalles = [];
          this.participantes.forEach((item: string) => {
            this.form.detalles.push({
              persona_uuid: item,
              calculo: 'A',
              monto: this.form.evento_costo,
            });
          });
          this.recalcularTotales();
        });
    },

    detalleNuevo(): void {
      // Antes de agregar un nuevo detalle, verifico que el que exista, tenga persona cargada.
      const ultimoDetalle = this.form.detalles.slice(-1);

      if (ultimoDetalle[0].persona_uuid === null) {
        Notify.create({
          type: 'info',
          timeout: 1000,
          message:
            'Debe definir un persona en detalle antes de agregar uno nuevo',
        });

        return; // finalizo el proceso en este punto
      }

      this.form.detalles.push({ ...nuevoDetalle }); //  usando { ...food } clono el objeto
      this.recalcularTotales();
    },

    detalleBorrar(index: number): void {
      // this.$reset();

      this.form.detalles.splice(index, 1);
      /*
      this.form.detalles.forEach((detalle) => {
        console.log(detalle);
      });
      console.log(this.form.detalles);
      */
    },

    recalcularTotales(): void {
      console.log('Recalclando totales');
      if (this.form.tipo_destinatario === 'P') {
        this.form.total = this.form.evento_costo;
      } else {
        // Si es tipo organismo

        this.form.total = 0; // Inicializo el contador
        this.form.detalles.forEach((item) => {
          if (item.calculo === 'A') {
            // Si el calculo del item es automatico, le pongo el valor correcto.
            item.monto = this.form.evento_costo;
          }

          this.form.total += Number(item.monto);
        });
      }
    },

    changeConceptoTipo(tipo: number) {
      // Tipo 2 es Eventos Formacion ->  se calcula todos los costos automaticamente.
      if (tipo === 2) {
        this.form.calculo = 'A';
        this.changeCalculo('A');
        return; // Finalizo la funcion
      }

      this.form.calculo = 'M';
      this.changeCalculo('M');
    },

    changeCalculo(calculo: string) {
      this.form.detalles.forEach((item) => (item.calculo = calculo));
      // Tipo 2 es Eventos Formacion ->  se calcula todos los costos automaticamente.
      if (calculo === 'A') {
        this.recalcularTotales();
        console.log('Calculo Automatico');
        return;
      }
      console.log('Calculo Manual');
    },

    // Funcion que disparo cada vez que se cambia el evento seleccionado
    changeEvento(evento_id: number) {
      const item = this.combos.eventos.filter((item) => item.id == evento_id);
      this.form.evento_costo = item[0].costo;

      if (this.form.calculo === 'A') {
        this.recalcularTotales();
      }
    },

    submitCrearRecibo() {
      return api
        .post('/api/finanzas/recibos', this.form)
        .then(() => this.reiniciarFormulario());
    },
  },
});
