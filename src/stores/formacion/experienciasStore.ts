import { defineStore } from 'pinia';
import { QTableProps } from 'quasar';
import { showSuccessNotification } from 'src/functions/function-show-notifications';

interface FiltroInterface {
  // Paginado
  sortBy: string;
  descending: boolean;
  page: number;
  rowsPerPage: number;
  rowsNumber: number;
  // Busqueda
  query: string;
  tipo: string;
  estado: string;
}

export const experienciaListadoSelect = {
  tipo: [
    { value: 'mizona', label: 'Mi Zona' },
    { value: 'publicos', label: 'Otras Zonas' },
  ],
  estado: [
    { value: 'activos', label: 'Activos' },
    { value: 'archivados', label: 'Archivados' },
    // { value: 'borrados', label: 'Borrados' },
  ],
};

export const useExperienciasStore = defineStore('experiencias.experiencias', {
  persist: true, // defino que los datos son persistentes en el navergador
  state: () => ({
    filtroActivado: false,
    filtros: <FiltroInterface>{
      // Paginado
      sortBy: 'desc',
      descending: false,
      page: 1,
      rowsPerPage: 10, // revisar este campo porque quiero usar rowsPerPage ya que es el que usa quasar
      rowsNumber: 0,
      query: '',
      tipo: 'mizona',
      estado: 'activos',
    },
  }),
  getters: {
    parametros(state) {
      // rescribo varias varibles, porque al momento de enviarlas por get, necesito que los que son array se separen x coma (,)
      return {
        ...state.filtros,
      };
    },
  },
  actions: {
    setRowTotal(total: number) {
      this.filtros.rowsNumber = total;
    },
    setPaginar(props: QTableProps) {
      this.filtros = <FiltroInterface>{
        ...this.filtros,
        ...props.pagination,
      };
    },

    resetFiltros() {
      showSuccessNotification('¡Filtros Reinicializados!');
      this.$reset();
    },
  },
});
