import { defineStore } from 'pinia';
import { QTableProps } from 'quasar';
import { showSuccessNotification } from 'src/functions/function-show-notifications';
import {
  FiltroInicialInterface,
  FiltroInicialValores,
} from 'src/stores/Inicial';

interface FiltroInterface extends FiltroInicialInterface {
  numero: number | null;
  rango_desde: number | null;
  rango_hasta: number | null;
  organismos_id: string | null;
}

export const useCerificadosStore = defineStore('eventos.certificados', {
  persist: true, // defino que los datos son persistentes en el navergador
  state: () => ({
    filtroActivado: false,
    filtros: <FiltroInterface>{
      ...FiltroInicialValores,
      //---------------
      numero: null,
      rango_desde: null,
      rango_hasta: null,
      organismos_id: null,
    },
  }),
  getters: {
    parametros(state) {
      return {
        ...state.filtros,
      };
    },
  },
  actions: {
    setRowTotal(total: number) {
      this.filtros.rowsNumber = total;
    },
    setPaginar(props: QTableProps) {
      this.filtros = <FiltroInterface>{
        ...this.filtros,
        ...props.pagination,
      };
    },
    resetFiltros() {
      showSuccessNotification('¡Filtros Reinicializados!');
      this.$reset();
    },
  },
});
