import { defineStore } from 'pinia';
import { QTableProps } from 'quasar';
import { showSuccessNotification } from 'src/functions/function-show-notifications';

interface FiltroInterface {
  // Paginado
  sortBy: string;
  descending: boolean;
  page: number;
  rowsPerPage: number;
  rowsNumber: number;
  // Busqueda
  tipo: string;
  evento_uuid: string;
  categoria: string[];
  rama: string[];
  mizona: string | null;
  dieta: string[];
  query: string;
}

export const useFormacionParticipantesStore = defineStore(
  'formacion.participantes',
  {
    persist: true, // defino que los datos son persistentes en el navergador
    state: () => ({
      filtroActivado: false,
      filtros: <FiltroInterface>{
        // Paginado
        sortBy: 'desc',
        descending: false,
        page: 1,
        rowsPerPage: 10, // revisar este campo porque quiero usar rowsPerPage ya que es el que usa quasar
        rowsNumber: 0,
        // Busqueda
        tipo: 'altas',
        evento_uuid: '',

        categoria: [],
        rama: [],
        mizona: null,
        dieta: [],

        query: '',
      },
    }),
    getters: {
      parametros(state) {
        // rescribo varias varibles, porque al momento de enviarlas por get, necesito que los que son array se separen x coma (,)
        return {
          ...state.filtros,
          // rama: state.filtros.rama.join(','),
          // dieta: state.filtros.dieta.join(','),
        };
      },
    },
    actions: {
      setRowTotal(total: number) {
        this.filtros.rowsNumber = total;
      },
      setPaginar(props: QTableProps) {
        this.filtros = <FiltroInterface>{
          ...this.filtros,
          ...props.pagination,
        };
      },
      inicializar() {
        this.filtroActivado = false;
        this.filtros = {
          // Paginado
          sortBy: 'desc',
          descending: false,
          page: 1,
          rowsPerPage: 10,
          rowsNumber: 10,
          // Busqueda
          tipo: 'altas',
          evento_uuid: '',

          categoria: [],
          rama: [],
          mizona: null,
          dieta: [],
          query: '',
        };
      },

      resetFiltros() {
        showSuccessNotification('¡Filtros Reinicializados!');
        this.inicializar();
      },
    },
  }
);
