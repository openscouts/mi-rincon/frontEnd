import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/login',
    component: () => import('layouts/LoginLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Auth/LoginIndex.vue') },
    ],
  },
  {
    path: '/registro',
    component: () => import('layouts/LoginLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Auth/Registro.vue') },
    ],
  },

  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'logout', component: () => import('pages/Auth/LoginLogout.vue') },
      {
        path: '/',
        component: () => import('pages/Main/DashBoard/IndexPage.vue'),
      },
      {
        path: '/perfil',
        component: () => import('pages/Main/Perfil/IndexPage.vue'),
      },
      {
        /**************   MAIN   *******************/
        path: 'main',
        children: [
          {
            path: 'inscripciones',
            component: () => import('pages/Main/InscripcionesPage.vue'),
          },
          {
            path: 'inscripciones/:uuid',
            component: () => import('pages/Main/InscripcionesAvanzadoPage.vue'),
          },
          {
            path: 'miformacion',
            component: () => import('pages/Main/MiFormacionPage.vue'),
          },
        ],
      },

      {
        /**************   EVENTOS   *******************/
        path: 'eventos',
        children: [
          {
            path: 'administracion',
            children: [
              {
                path: '',
                meta: { acl: 'eventos.listados' },
                component: () =>
                  import('pages/Eventos/Administracion/ListadoPage.vue'),
              },
              {
                path: 'detalle/:uuid',
                meta: { acl: 'eventos.listados' },
                component: () =>
                  import(
                    'pages/Eventos/Administracion/Detalle/DetallePage.vue'
                  ),
              },
            ],
          },

          {
            path: 'creacion',
            children: [
              {
                path: '',
                meta: { acl: 'eventos.alta' },
                component: () =>
                  import('src/pages/Eventos/Creacion/NuevoPage.vue'),
              },
            ],
          },

          {
            path: 'editar',
            children: [
              {
                path: ':uuid',
                meta: { acl: 'eventos.edicion' },
                component: () =>
                  import('src/pages/Eventos/Creacion/EditarPage.vue'),
              },
            ],
          },

          {
            path: 'lugares',
            meta: { acl: 'eventos.lugares' },
            children: [
              {
                path: '',
                meta: { acl: 'eventos.lugares' },
                component: () => import('pages/Eventos/Lugares/IndexPage.vue'),
              },
              {
                path: 'editar/:uuid',
                meta: { acl: 'eventos.lugares.edicion' },
                component: () => import('pages/Eventos/Lugares/EditarPage.vue'),
              },
              {
                path: 'nuevo',
                meta: { acl: 'eventos.lugares.alta' },
                component: () => import('pages/Eventos/Lugares/NuevoPage.vue'),
              },
            ],
          },
        ],
      },

      {
        /**************   EVENTOS   *******************/
        path: 'formacion',
        children: [
          {
            path: 'carga_manual',
            children: [
              {
                path: '',
                meta: { acl: 'XXXXXXXXXXX' },
                component: () =>
                  import('pages/Formacion/CargaManual/IndexPage.vue'),
              },
            ],
          },

          {
            path: 'cursantes',
            children: [
              {
                path: '',
                meta: { acl: 'formacion.cursantes' },
                component: () =>
                  import('pages/Formacion/Cursantes/IndexPage.vue'),
              },

              {
                path: 'detalle/:uuid',
                meta: { acl: 'formacion.cursantes' },
                component: () =>
                  import('pages/Formacion/Cursantes/Detalle/IndexPage.vue'),
              },
            ],
          },

          {
            path: 'certificados',
            children: [
              {
                path: '',
                meta: { acl: 'formacion.cursantes' },
                component: () =>
                  import('pages/Formacion/Certificados/IndexPage.vue'),
              },
            ],
          },

          {
            path: 'formadores',
            children: [
              {
                path: '',
                meta: { acl: 'formacion.formadores' },
                component: () =>
                  import('pages/Formacion/Formadores/IndexPage.vue'),
              },
            ],
          },

          {
            path: 'experiencias',
            children: [
              {
                path: '',
                meta: { acl: 'formacion.experiencias' },
                component: () =>
                  import('pages/Formacion/Experiencias/IndexPage.vue'),
              },
              {
                path: 'listado',
                meta: { acl: 'formacion.experiencias' },
                component: () =>
                  import('pages/Formacion/Experiencias/ListadoPage.vue'),
              },
              {
                path: 'detalle/:uuid',
                meta: { acl: 'formacion.experiencias' },
                component: () =>
                  import('pages/Formacion/Experiencias/Detalle/IndexPage.vue'),
              },
              {
                path: 'detalle/:uuid/edicion_masiva',
                meta: { acl: 'formacion.experiencias.edicion' },
                component: () =>
                  import(
                    'pages/Formacion/Experiencias/Detalle/EdicionMasivaPage.vue'
                  ),
              },
            ],
          },
          {
            path: 'insignia_de_madera',
            children: [
              {
                path: '',
                meta: { acl: 'formacion.insignia_de_madera' },
                component: () =>
                  import('pages/Formacion/InsigniaDeMadera/IndexPage.vue'),
              },
              {
                path: 'finalizar/:uuid',
                meta: { acl: 'formacion.insignia_de_madera.confirmacion' },
                component: () =>
                  import('pages/Formacion/InsigniaDeMadera/EditarPage.vue'),
              },

              {
                path: 'editar/:uuid',
                meta: { acl: 'formacion.insignia_de_madera.edicion' },
                component: () =>
                  import('pages/Formacion/InsigniaDeMadera/EditarPage.vue'),
              },
              {
                path: 'nuevo',
                meta: { acl: 'formacion.insignia_de_madera.alta' },
                component: () =>
                  import('pages/Formacion/InsigniaDeMadera/NuevoPage.vue'),
              },
              {
                path: 'listado',
                meta: { acl: 'formacion.insignia_de_madera' },
                component: () =>
                  import('pages/Formacion/InsigniaDeMadera/ListadoPage.vue'),
              },
            ],
          },
        ],
      },

      {
        /**************   MEMBRESIA   *******************/
        path: 'membresia',
        children: [
          {
            path: 'personas/',
            meta: { acl: 'membresia.personas' },
            component: () => import('pages/Membresia/Personas/IndexPage.vue'),
          },

          {
            path: 'personas/listado',
            meta: { acl: 'membresia.personas' },
            component: () => import('pages/Membresia/Personas/ListadoPage.vue'),
          },

          {
            path: 'afiliacion',
            children: [
              {
                path: '',
                meta: { acl: 'membresia.afiliacion' },
                component: () =>
                  import('pages/Membresia/Afiliacion/IndexPage.vue'),
              },
              {
                path: 'carga',
                meta: { acl: 'membresia.afiliacion' },
                component: () =>
                  import('pages/Membresia/Afiliacion/CargaPage.vue'),
              },
            ],
          },

          {
            path: 'funciones',
            children: [
              {
                path: '',
                meta: { acl: 'membresia.funciones' },
                component: () =>
                  import('pages/Membresia/Funciones/IndexPage.vue'),
              },
              {
                path: 'alta',
                meta: { acl: 'membresia.funciones.alta' },
                component: () =>
                  import('pages/Membresia/Funciones/AltaPage.vue'),
              },
            ],
          },

          {
            path: 'personas/perfil/:uuid',
            meta: { acl: 'membresia.personas.legajos' },
            component: () =>
              import('pages/Membresia/Personas/Perfil/IndexPage.vue'),
          },
        ],
      },

      {
        path: 'finanzas',
        children: [
          {
            path: 'recibos',
            children: [
              {
                path: '',
                meta: { acl: 'finanzas.recibos' },
                component: () => import('pages/Finanzas/Recibos/IndexPage.vue'),
              },
              {
                path: 'nuevo',
                meta: { acl: 'finanzas.recibos.alta' },
                component: () =>
                  import('pages/Finanzas/Recibos/Nuevo/IndexPage.vue'),
              },

              {
                path: 'listado',
                meta: { acl: 'finanzas.recibos' },
                component: () =>
                  import('pages/Finanzas/Recibos/ListadoPage.vue'),
              },
              {
                path: 'detalle/:uuid',
                meta: { acl: 'finanzas.recibos' },
                component: () =>
                  import('pages/Finanzas/Recibos/DetallePage.vue'),
              },
            ],
          },
        ],
      },

      {
        path: 'administrador',
        children: [
          {
            path: 'jobs',
            meta: { acl: 'administrador.permisos' },
            component: () => import('pages/Administrador/Jobs/IndexPage.vue'),
          },
          {
            path: 'novedades',
            meta: { acl: 'administrador.novedades' },

            children: [
              {
                path: '',
                meta: { acl: 'administrador.novedades' },
                component: () =>
                  import('pages/Administrador/Novedades/IndexPage.vue'),
              },
              {
                path: 'alta',
                meta: { acl: 'administrador.novedades' },
                component: () =>
                  import('pages/Administrador/Novedades/NuevoPage.vue'),
              },
              {
                path: 'editar/:uuid',
                meta: { acl: 'administrador.novedades' },
                component: () =>
                  import('pages/Administrador/Novedades/EditarPage.vue'),
              },
            ],
          },
          {
            path: 'permisos',
            meta: { acl: 'administrador.permisos' },
            component: () =>
              import('pages/Administrador/Permisos/IndexPage.vue'),
          },
          {
            path: 'registros',
            meta: { acl: 'administrador.usuarios.edicion' },
            component: () =>
              import('pages/Administrador/Registros/IndexPage.vue'),
          },
          {
            path: 'usuarios',
            meta: { acl: 'administrador.usuarios' },
            component: () =>
              import('pages/Administrador/Usuarios/IndexPage.vue'),
          },
        ],
      },
    ],
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
